﻿using BuissnessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Dal
    {
        Context db = new Context();

        public int AddUser(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
            return 0;
        }
        public int DeleteUser(string name)
        {
            User user = db.Users.Find(name);
            db.Users.Remove(user);
            return 0;
        }

        public int AddProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
            return 0;
        }
        public int DeleteProduct(string name)
        {
            Product product = db.Products.Find(name);
            db.Products.Remove(product);
            db.SaveChanges(true);
            return 0;
        }
        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.ProductName == name).FirstOrDefault();
            if (obj!=null)
            {
                if(product.ProductName.Length>0)
                obj.ProductName = product.ProductName;
                if (product.Price>0)
                    obj.Price = product.Price;

                if (product.Quantity> 0)
                    obj.Quantity = product.Quantity;    

                db.Products.Update(obj);
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 2;
            }
            
            

        }
        public User GetUser(string name)
        {
            User user = db.Users.Where(x=>x.FirstName==name).FirstOrDefault();
            return user;
        }
        public Product GetProduct(string name)
        {
            //Product product = db.Products.Find(name);
            Product product = db.Products.Where(x=>x.ProductName == name).FirstOrDefault();
            return product;

        }
        public int AddOrder(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return 1;
        }
        public List<Product> AllProducts()
        {
            List<Product> products = new List<Product>();
            var list = db.Products.ToList();
            foreach(Product product in list)
                products.Add(product);

            return products;
        }
        public Role GetRole(int id)
        {
            Role role= db.Roles.Find(id);
            return role;
        }
    }
}
