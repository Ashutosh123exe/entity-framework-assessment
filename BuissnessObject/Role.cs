﻿using System.ComponentModel.DataAnnotations;

namespace BuissnessObject
{
    public class Role
    {
        [Range(0,2)]
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }
}