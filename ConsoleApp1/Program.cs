﻿using BAL;
using BuissnessObject;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string s = "yes";
            Bal l = new Bal();
            while(s.Equals("yes"))
            {
                Console.WriteLine(" Enter 0  to continue as Admin\n Enter 1 to continue as Supplier\n Enter 2 to Continue as Customer");
                int choice = Convert.ToInt32(Console.ReadLine());
                if(choice==0)
                {
                    Console.WriteLine(" Enter 1 to add user , 2 to delete user");
                    choice=Convert.ToInt32(Console.ReadLine());
                    if (choice == 1)
                    {
                        Console.WriteLine("Enter First Name");
                        string name = Console.ReadLine();
                        Console.WriteLine("Enter Last Name");
                        string Lname = Console.ReadLine();
                        Console.WriteLine("Enter 2 to add Supplier and 3 to the Customer type of User");
                        int type = Convert.ToInt32(Console.ReadLine());
                        Role role = l.GetRole(type);
                        User user = new User()
                        {
                            FirstName = name,
                            LastName = Lname,
                            Role = role,
                        };
                        l.AddUser(user);
                    }
                    else if (choice == 2)
                    {
                        Console.WriteLine("Enter the name of the user you want to delete");
                        string a= Console.ReadLine();
                        l.DeleteUser(a);
                    }
                    else
                    {
                        Console.WriteLine("Wrong choice");
                    }
                       

                    }
                else if (choice == 1)
                {
                    Console.WriteLine(" Enter 1 to add product\n Enter 2 to delete product\n Enter 3 to update product");
                    choice = Convert.ToInt32(Console.ReadLine());
                    if(choice == 1)
                    {
                        Console.WriteLine("Enter product name");
                        string name= Console.ReadLine();
                        Console.WriteLine("Enter price of the product");
                        int price = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter the quantity of the product");
                        int quantity = Convert.ToInt32(Console.ReadLine());
                        
                        Product product = new Product()
                        {
                            ProductName = name,
                            Price = price,
                            Quantity = quantity,
                            
                        };
                        l.AddProduct(product);

                    }
                    else if(choice == 2)
                    {
                        Console.WriteLine("Enter the name of the product you want to delete");
                        string name = Console.ReadLine();
                        l.DeleteProduct(name);
                    }
                    else if(choice == 3){
                        Console.WriteLine("Enter the name of the product to edit");
                        string name = Console.ReadLine();
                        Product product = l.GetProduct(name);
                        if(product != null)
                        {
                            Console.WriteLine("Enter the new name");
                            string? ProductName = Console.ReadLine();
                            Console.WriteLine("Enter the new price");
                            //int Price = Convert.ToInt32(Console.ReadLine());
                            int Price;
                            Int32.TryParse(Console.ReadLine(), out Price);
                            Console.WriteLine("Enter the new Quantity");
                            int Quantity;
                            Int32.TryParse(Console.ReadLine(), out Quantity);
                            Product upDatedProduct = new Product()
                            {
                                ProductName = ProductName,
                                Price = Price,
                                Quantity = Quantity
                            };
                            if(l.UpdateProduct(name,upDatedProduct)==1)
                            {
                                Console.WriteLine("Product added successfully");
                            }
                            else if(l.UpdateProduct(name, upDatedProduct) == 2)
                            {
                                Console.WriteLine("product doesn't exsists");
                            }
                        }
                    }
                }
                else if (choice == 2)
                {
                    Console.WriteLine("Enter 1 to get all products\n Enter 2 to add Order");
                     choice=Convert.ToInt32(Console.ReadLine());
                    if(choice == 1)
                    {
                        Console.WriteLine("Available products are");
                        var list = l.AllProducts();
                        int i = 0;
                        foreach(Product product in list)
                        {
                            Console.WriteLine("-------------Product"+ (i++)+ "----------");
                            Console.WriteLine(product.ProductId);
                            Console.WriteLine(product.ProductName);
                            Console.WriteLine(product.Price);
                            Console.WriteLine(product.Quantity);
                        }
                    }
                    else if (choice == 2)
                    {
                        Console.WriteLine("Enter the name of the Product u want to add");
                        string name = Console.ReadLine();
                        Product product= l.GetProduct(name);
                        Console.WriteLine("Enter User name");
                        string fname=Console.ReadLine();
                        User user = l.GetUser(fname);
                        Order order = new Order()
                        {
                            User = user,
                            Product = product,
                        };
                        l.AddOrder(order);
                    }
                    else
                    {
                        Console.WriteLine("Wrong choice");
                    }

                }
                else
                {
                    Console.WriteLine("Wrong Choice");
                }
                Console.WriteLine("Enter yes to continue");
                s = Console.ReadLine();
            }
            

            }

            
        }
    }
