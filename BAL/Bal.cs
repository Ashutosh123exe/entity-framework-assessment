﻿using BuissnessObject;
using DAL;

namespace BAL
{
    public class Bal
    {
        Dal dal = new Dal();
        public int AddUser(User user)
        {
            dal.AddUser(user);
            return 1;
        }
        public int DeleteUser(string name)
        {
            dal.DeleteUser(name);
            return 1;
        }
        public int AddProduct(Product product)
        {
            dal.AddProduct(product);
            return 1;

        }
        public int DeleteProduct(string name)
        {
            dal.DeleteProduct(name);
            return 1;
        }
        public int UpdateProduct(string name,Product product)
        {
          return  dal.UpdateProduct(name,product);
                
        }
        public User GetUser(string name)
        {
            return dal.GetUser(name);
                
        }
        public Product GetProduct(string name)
        {
            return dal.GetProduct(name);
        }
        public string AddOrder(Order order)
        {
            dal.AddOrder(order);
            
            return("Product has been added") ;
        }
        public List<Product> AllProducts()
        {
           return dal.AllProducts();
        }
        public Role GetRole(int roleid)
        {
            return dal.GetRole(roleid);
        }
    }
}